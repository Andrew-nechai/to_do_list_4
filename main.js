window.onload = function () {
var check_svg = '<svg  version="1.1" id="Layer_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"viewBox="0 0 426.67 426.67" style="enable-background:new 0 0 426.67 426.67;" xml:space="preserve"><path  id="check" d="M153.504,366.839c-8.657,0-17.323-3.302-23.927-9.911L9.914,237.265c-13.218-13.218-13.218-34.645,0-47.863c13.218-13.218,34.645-13.218,47.863,0l95.727,95.727l215.39-215.386c13.218-13.214,34.65-13.218,47.859,0c13.222,13.218,13.222,34.65,0,47.863L177.436,356.928C170.827,363.533,162.165,366.839,153.504,366.839z"/></svg>';
var uncheck_svg = '<svg  version="1.1" id="Layer_2" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"viewBox="0 0 426.67 426.67" style="enable-background:new 0 0 426.67 426.67;" xml:space="preserve"><path  id="uncheck" d="M153.504,366.839c-8.657,0-17.323-3.302-23.927-9.911L9.914,237.265c-13.218-13.218-13.218-34.645,0-47.863c13.218-13.218,34.645-13.218,47.863,0l95.727,95.727l215.39-215.386c13.218-13.214,34.65-13.218,47.859,0c13.222,13.218,13.222,34.65,0,47.863L177.436,356.928C170.827,363.533,162.165,366.839,153.504,366.839z"/></svg>';
var REMOVE_SVG = '<svg  viewBox="0 0 496 496" xmlns="http://www.w3.org/2000/svg"><path d="m488 0h-480c-4.417969 0-8 3.582031-8 8v120c0 4.417969 3.582031 8 8 8h17.441406l70.71875 353.601562c.761719 3.722657 4.039063 6.398438 7.839844 6.398438h288c3.800781 0 7.078125-2.675781 7.839844-6.398438l70.71875-353.601562h17.441406c4.417969 0 8-3.582031 8-8v-120c0-4.417969-3.582031-8-8-8zm-102.558594 480h-274.882812l-68.796875-344h412.476562zm94.558594-360h-464v-104h464zm0 0"/><path id = "clear" d="m138.175781 360.566406c0 2.125.84375 4.15625 2.34375 5.65625l45.257813 45.257813c3.121094 3.121093 8.1875 3.121093 11.3125 0l50.910156-50.914063 50.910156 50.914063c3.125 3.121093 8.191406 3.121093 11.3125 0l45.257813-45.257813c3.121093-3.121094 3.121093-8.1875 0-11.3125l-50.914063-50.910156 50.914063-50.910156c3.121093-3.125 3.121093-8.191406 0-11.3125l-45.257813-45.257813c-3.121094-3.121093-8.1875-3.121093-11.3125 0l-50.910156 50.914063-50.910156-50.914063c-3.125-3.121093-8.191406-3.121093-11.3125 0l-45.257813 45.257813c-3.121093 3.121094-3.121093 8.1875 0 11.3125l50.914063 50.910156-50.914063 50.910156c-1.5 1.5-2.34375 3.535156-2.34375 5.65625zm70.222657-50.910156c3.125-3.125 3.125-8.1875 0-11.3125l-50.910157-50.910156 33.945313-33.9375 50.910156 50.902344c3.125 3.125 8.1875 3.125 11.3125 0l50.910156-50.902344 33.945313 33.9375-50.910157 50.910156c-3.125 3.125-3.125 8.1875 0 11.3125l50.910157 50.910156-33.945313 33.9375-50.910156-50.902344c-3.125-3.125-8.1875-3.125-11.3125 0l-50.910156 50.902344-33.945313-33.9375zm0 0"/></svg>';

var mass = []

	document.getElementById('add').addEventListener("click", function() {
		var task = document.getElementById('input').value;
		addtask(task);
		for (var i in mass) {
			list.insertBefore(item, list.childNodes[0]);
		}
		document.getElementById('input').value = '';
	});

//добавление дела
function addtask(smth) {
	var item_add = {};
	item_add.existence = true;
	item_add.checked = false;
	var list = document.getElementById('todo');

	var item = document.createElement('li');
	item.innerText = smth;

	var but_check = document.createElement('button');
	but_check.classList.add('check'); 
	but_check.innerHTML = check_svg;
	but_check.addEventListener('click',function(){
		but_check.classList.toggle("uncheck");
		but_check.classList.toggle("check");
		item_add.checked = (but_check.classList.value == "check") ? "check":"uncheck";
		let y = this.parentNode;
		if (but_check.classList == "uncheck") {
			y.style.textDecoration = 'line-through'
		} else {
			y.style.textDecoration = 'none'
		}
		if (but_check.classList.contains("uncheck")) {
	 		but_check.innerHTML = uncheck_svg; 
	 		item_add.check = false;
		} else { if (but_check.classList.contains("check")) {
			but_check.innerHTML = check_svg;
			item_add.check = false;
		}}
	});

	var but_remove = document.createElement('button');
	but_remove.classList.add('clear');
	but_remove.innerHTML = REMOVE_SVG;
	but_remove.addEventListener('click', function () {
		
		var ul = document.getElementById('todo');
		var trash = this.parentNode; 
		item_add.existence = false;
		ul.removeChild(trash);
	});

	item.appendChild(but_check);
	item.appendChild(but_remove);
	mass.push(item_add);	
}
}